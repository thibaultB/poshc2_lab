for sleep in 60s 600s 1800s 3600s
do
    for jitter in 0.0 0.1 0.2 0.3 0.4 0.5 0.6
    do
        bash launch_one_simulation.sh $sleep $jitter;
    done;
done;
