Generation of HTTP C2 network traffic with sleep_time and jitter configurable. The biggest value added by this project is the possibility to script a set of commands that will be send by the C2 server to the victim.

# Usage

Just use 
```bash
bash launch_one_simulation.sh <sleep time  in seconds: 15s 600s>  <jitter in float format: 0.1 0.5>
```

If you want your C2 to execute commands on the client, juste modify the **config/commands.conf** and add each command as a new line. **An empty line = no commands for this beacon occurrence**
The C2 serveur will execute each command, waiting the defined sleep time between executing the next command.

# Optional

**launch_multiple_simulation.sh** is an example of a script that can be used to generate simulation for multiple configurations of sleep_time and jitter.

# Output

Each call to launch_one_simulation.sh will generate a .pcap file in the current directory.

**Warning:** The pcap file can be truncated in the middle of a packet because the docker timeout after a predefined time (could be changed in launch_one_simulation.sh script).
If it happen to your file, you can correct it using tcpdump -r <yourfile.pcap> -w <yourfilecorrected.pcap>



